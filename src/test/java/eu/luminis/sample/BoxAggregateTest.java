package eu.luminis.sample;

import eu.luminis.sample.commands.*;
import eu.luminis.sample.events.BoxCreatedEvent;
import eu.luminis.sample.events.BoxReturnedEvent;
import eu.luminis.sample.events.BoxShippedEvent;
import eu.luminis.sample.exceptions.AlreadyReturnedException;
import eu.luminis.sample.exceptions.AlreadyShippedException;
import eu.luminis.sample.services.BrickScanner;
import eu.luminis.sample.values.Address;
import eu.luminis.sample.values.Location;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BoxAggregateTest {
    private final UUID millenniumFalconLegoSet = UUID.randomUUID();
    private final Address campusAddress = new Address("One Infinite Loop", "CA 95014", "Cupertino", "United States");
    private final Address operaHouseAddress = new Address("2 Macquarie Street", "NSW 2000", "Sydney", "Australia");

    @Disabled // strange API : we cannot create empty aggregates
    @Test
    public void shouldCreateOrder() {
        var event = new BoxAggregate(List.of()).handle(
                new CreateOrderCommand(List.of(millenniumFalconLegoSet), campusAddress)
        );

        assertEquals(campusAddress, event.shippingAddress());
    }

    @Test
    public void shouldFailWhenMissingCreatedEvent() {
        assertThrows(IllegalStateException.class, () ->
                new BoxAggregate(List.of())
        );
    }

    @Test
    public void shouldAllowChangingShippingAddressWhenNotShipped() {
        var order = new BoxAggregate(List.of(
                new BoxCreatedEvent(Instant.now(), List.of(millenniumFalconLegoSet), campusAddress)
        ));

        var event = order.handle(
                new ChangeShippingAddressCommand(operaHouseAddress)
        );

        assertEquals(operaHouseAddress, event.shippingAddress());
    }

    @Test
    public void shouldNotAllowShippingOrderWhenAlreadyShipped() {
        var order = new BoxAggregate(List.of(
                new BoxCreatedEvent(Instant.now(), List.of(millenniumFalconLegoSet), campusAddress),
                new BoxShippedEvent(Instant.now(), "UPS")
        ));

        assertThrows(AlreadyShippedException.class, () ->
                order.handle(new ShipOrderCommand("UPS"))
        );
    }

    @Test
    public void shouldNotAllowChangingShippingOrderAfterBeingShipped() {
        var order = new BoxAggregate(List.of(
                new BoxCreatedEvent(Instant.now(), List.of(millenniumFalconLegoSet), campusAddress),
                new BoxShippedEvent(Instant.now(), "UPS")
        ));

        assertThrows(AlreadyShippedException.class, () ->
                order.handle(new ChangeShippingAddressCommand(operaHouseAddress))
        );
    }

    @Test
    public void locationShouldBeClientAfterBoxIsShipped() {
        // Given
        var box = new BoxAggregate(List.of(
                new BoxCreatedEvent(Instant.now(), List.of(millenniumFalconLegoSet), campusAddress)
        ));

        // When
        var result = box.handle(new ShipOrderCommand("UPS"));

        // Then
        assertEquals(Location.CLIENT, box.getLocation());
    }

    @Test
    public void shouldAllowToReturnShippedBox() {
        // Given
        var box = new BoxAggregate(List.of(
                new BoxCreatedEvent(Instant.now(), List.of(millenniumFalconLegoSet), campusAddress),
                new BoxShippedEvent(Instant.now(), "UPS")
        ));

        // When
        var result = box.handle(new ReturnBoxCommand("aaa"));

        // Then
        assertEquals(Location.OFFICE, box.getLocation());
    }

    @Test
    public void shouldNotAllowToReturnTwiceABox() {
        // Given
        var box = new BoxAggregate(List.of(
                new BoxCreatedEvent(Instant.now(), List.of(millenniumFalconLegoSet), campusAddress),
                new BoxShippedEvent(Instant.now(), "UPS"),
                new BoxReturnedEvent("aaa")
        ));

        // When-Then
        assertThrows(AlreadyReturnedException.class, () ->
                box.handle(new ReturnBoxCommand("aaa"))
        );

    }

    @Test
    public void shouldScanBoxContents() {
        // Given
        var box = new BoxAggregate(List.of(
                new BoxCreatedEvent(Instant.now(), List.of(millenniumFalconLegoSet), campusAddress),
                new BoxShippedEvent(Instant.now(), "UPS"),
                new BoxReturnedEvent("aaa")
        ));
        var brickScanner = new DummyBrickScanner(2);

        // When
        var result = box.handle(new BoxContentsScanCommand(), brickScanner);

        // Then
        assertEquals(2, box.getMissingPieces());
    }

    @Test
    // only scan box that is in the office

    record DummyBrickScanner(
            int fixedValue
    ) implements BrickScanner {
        @Override
        public int scan() {
            return fixedValue;
        }
    }

}

