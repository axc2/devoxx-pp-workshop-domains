package eu.luminis.sample.services;

public interface BrickScanner {
    int scan();
}
