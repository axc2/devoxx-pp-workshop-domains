package eu.luminis.sample.events;

import java.time.Instant;

public record BoxShippedEvent(
        Instant dateShipped,
        String shippingProvider
) implements BoxEvent {
}
