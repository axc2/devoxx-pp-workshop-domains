package eu.luminis.sample.events;

public record BoxContentsScannedEvent(
        int missingPieces
) implements BoxEvent {
}
