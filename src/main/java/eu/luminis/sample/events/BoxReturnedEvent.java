package eu.luminis.sample.events;

public record BoxReturnedEvent(
        String boxNumber
) implements BoxEvent {
}
