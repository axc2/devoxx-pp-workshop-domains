package eu.luminis.sample.events;

public sealed interface BoxEvent permits BoxContentsScannedEvent, BoxCreatedEvent, BoxReturnedEvent, BoxShippedEvent, ShippingAddressChangedEvent {
}

