package eu.luminis.sample;

import eu.luminis.sample.commands.*;
import eu.luminis.sample.events.*;
import eu.luminis.sample.exceptions.AlreadyReturnedException;
import eu.luminis.sample.exceptions.AlreadyShippedException;
import eu.luminis.sample.services.BrickScanner;
import eu.luminis.sample.values.Location;

import java.time.Instant;
import java.util.List;

public class BoxAggregate {

    private boolean hasBeenShipped;
    private Location location;
    private int missingPieces;

    public Location getLocation() {
        return location;
    }

    public int getMissingPieces() {
        return missingPieces;
    }

    public BoxAggregate(List<BoxEvent> history) {
        if (!firstEventIsOrderCreated(history)) {
            throw new IllegalStateException("Missing order created event");
        }

        applyStateFrom(history);
    }

    public BoxCreatedEvent handle(CreateOrderCommand command) {
        var emittedEvent = new BoxCreatedEvent(Instant.now(), command.items(), command.shippingAddress());

        applyNewEvent(emittedEvent);
        return emittedEvent;
    }

    public ShippingAddressChangedEvent handle(ChangeShippingAddressCommand command) {
        if (hasBeenShipped) throw new AlreadyShippedException();

        var emittedEvent = new ShippingAddressChangedEvent(command.shippingAddress());

        applyNewEvent(emittedEvent);
        return emittedEvent;
    }

    public BoxShippedEvent handle(ShipOrderCommand command) {
        if (hasBeenShipped) throw new AlreadyShippedException();

        var emittedEvent = new BoxShippedEvent(Instant.now(), command.shippingProvider());

        applyNewEvent(emittedEvent);
        return emittedEvent;
    }

    public BoxReturnedEvent handle(ReturnBoxCommand command) {
        if (location != Location.CLIENT && hasBeenShipped) throw new AlreadyReturnedException();

        var emittedEvent = new BoxReturnedEvent("bla");

        applyNewEvent(emittedEvent);
        return emittedEvent;
    }

    public BoxContentsScannedEvent handle(BoxContentsScanCommand command, BrickScanner brickScanner) {
        var emittedEvent = new BoxContentsScannedEvent(
                brickScanner.scan()
        );

        applyNewEvent(emittedEvent);
        return emittedEvent;
    }

    private void applyStateFrom(List<BoxEvent> events) {
        for (BoxEvent e : events) {
            applyNewEvent(e);
        }
    }

    private void applyNewEvent(BoxEvent e) {
        switch (e) {
            case BoxCreatedEvent event -> {
                location = Location.OFFICE;
            }
            case ShippingAddressChangedEvent event -> {
            }
            case BoxShippedEvent event -> {
                location = Location.CLIENT;
                hasBeenShipped = true;
            }
            case BoxReturnedEvent boxReturnedEvent -> {
                location = Location.OFFICE;
            }
            case BoxContentsScannedEvent boxContentsScannedEvent -> {
                missingPieces = boxContentsScannedEvent.missingPieces();
            }
        }
    }

    private static boolean firstEventIsOrderCreated(List<BoxEvent> history) {
        return !history.isEmpty() && history.get(0) instanceof BoxCreatedEvent;
    }
}
