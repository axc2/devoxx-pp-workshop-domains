package eu.luminis.sample.commands;

public record ShipOrderCommand(String shippingProvider) {
}
