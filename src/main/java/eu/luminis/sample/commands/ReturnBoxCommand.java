package eu.luminis.sample.commands;

public record ReturnBoxCommand(
        String boxNumber
) {

}
